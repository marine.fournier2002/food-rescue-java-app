# Food Rescue

A Food Rescue consiste num organizador de data de validade dos ingredientes obtidos a partir da leitura digital das faturas do supermercado. Com estes ingredientes e respectivas datas de validade a aplicação sugere receitas e avisa quando esta data está próxima de expirar. Desta forma qualquer utilizador consegue gerir os ingredientes que têm em casa, ao mesmo tempo que aproveita sugestões de receitas para não os desperdiçar. A Food Rescue foi desenvolvida recorrendo à linguagem de programação java e ao FrameWork Spring boot.

## Requisitos

- Java 11 ou superior instalado
- Maven
- IDE (Como IntelliJ)


## Como Instalar

1. Clone este repositório:

   ```bash
   git clone https://gitlab.com/marine.fournier2002/food-rescue-java-app.git
   cd seu-repositorio
   ./mvnw spring-boot:run


# Nota 
  
  No caso em que, ao ser corrido, o código der um erro relaciondo com a base de dados, talves seja necessário criar uma nova base de dados local ou num servidor. Esta alteração pode ser feita na "aplication.proprieties" com um servior como o Railwey.



